﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Attack")]
public class AttackAction : Action {

	public GameObject owner;

    public GameObject target;

	[SerializeField]
	public string attackAnimation;

	[SerializeField]
	public int manaCost;

	public override void Act (StateController controller) {
            Hit(controller);
        }
	
	public void Hit(StateController controller) {
		UnitStats ownerStats = this.owner.GetComponent<UnitStats> ();
		UnitStats targetStats = target.GetComponent<UnitStats> ();
        int damageDealt = 0;
        int extraDamage = 0;

       int attackCalculator = Random.Range(1, 20) + ownerStats.attackBonus;

        if(attackCalculator > targetStats.defenseScore) {
            damageDealt = ownerStats.isMagic ? Random.Range(1, 6) + ownerStats.attackBonus : Random.Range(1, 12) + ownerStats.attackBonus;
            extraDamage = !ownerStats.isActiveAndEnabled ? Random.Range(1, 8) : 0;
        }
        else {
            damageDealt = 0;
        }

		this.owner.GetComponent<Animator> ().Play (this.attackAnimation);

		targetStats.receiveDamage (damageDealt + extraDamage);
	}
}
