using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour {

    public State currentState;
    public UnitStats unitStats;
    public State remainState;
    private bool aiActive;


    void Awake () {

    }

    public void SetupAI(bool aiActive) {
        if (aiActive) {
            currentState.UpdateState (this);
        } else {
            return;
        }
    }

    public void TransitionToState(State nextState) {
        if (nextState != remainState) {
            currentState = nextState;
            OnExitState ();
        }
    }

    private void OnExitState() {
        // stateTimeElapsed = 0;
    }
}