using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/MagicAttack")]

public class MagicAttackAction : Action {
    public override void Act (StateController controller) {
        MagicAttack (controller);
    }

    public GameObject owner;

    public GameObject target;

    [SerializeField]
	public string attackAnimation;

	[SerializeField]
	public int manaCost;

    private void MagicAttack(StateController controller) {
        UnitStats ownerStats = this.owner.GetComponent<UnitStats>();
        UnitStats targetStats = target.GetComponent<UnitStats>();
        int damageDealt = 0;

        if (ownerStats.mana >= this.manaCost) {
            int attackCalculator = Random.Range(1, 20) + ownerStats.attackBonus;

            if (attackCalculator > targetStats.defenseScore) {
            damageDealt = ownerStats.isMagic ? Random.Range(8, 48) : Random.Range(1, 8);
            }
            else {
                damageDealt = 0;
            }

            this.owner.GetComponent<Animator>().Play(this.attackAnimation);

            targetStats.receiveDamage(damageDealt);

            ownerStats.mana -= this.manaCost;
        }
    }
}