﻿using UnityEngine;

public class KillDamageText : MonoBehaviour {

	[SerializeField]
	public float destroyTime;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, this.destroyTime);
	}
	
	void OnDestroy() {
		GameObject turnSystem = GameObject.Find ("TurnSystem");
		turnSystem.GetComponent<TurnSystem> ().nextTurn ();
	}
}
