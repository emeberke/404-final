﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class UnitStats : MonoBehaviour, IComparable {

	[SerializeField]
	public Animator animator;

	[SerializeField]
	public GameObject damageTextPrefab;

	[SerializeField]
	public Vector2 damageTextPosition;

    [SerializeField]
    public bool isMagic;

    public int maxHealth;
	public int currentHealth;
	public int mana;
    public int wisdom;
    public int dexterity;
	public int attackBonus;
    public int baseAC;
	public int defenseScore;
	public float speed;

	public int nextActTurn;

	private bool dead = false;

	private float currentExperience;

	void Start() {
		
	}

    public void defending(bool defender) {
        GameObject HUDCanvas = GameObject.Find("HUDCanvas");
        GameObject damageText = Instantiate(this.damageTextPrefab, HUDCanvas.transform) as GameObject;

        string flavorText = (defender) ? "defend" : "look";

        damageText.GetComponent<Text>().text = flavorText;

        damageText.transform.localPosition = this.damageTextPosition;
        damageText.transform.localScale = new Vector2(1.0f, 1.0f);

        
    }

    public void receiveDamage(int damage) {
        this.currentHealth -= damage;
        animator.Play("Hit");

        GameObject HUDCanvas = GameObject.Find("HUDCanvas");
        GameObject damageText = Instantiate(this.damageTextPrefab, HUDCanvas.transform) as GameObject;
        if (damage == 0) {
            damageText.GetComponent<Text>().text = "miss";
        }
        else {
            damageText.GetComponent<Text>().text = "" + damage;
        }

        if (baseAC != defenseScore) {
            defenseScore -= 5;
        }

        damageText.transform.localPosition = this.damageTextPosition;
        damageText.transform.localScale = new Vector2(1.0f, 1.0f);

        if (this.currentHealth <= 0) {
            this.dead = true;
            this.gameObject.tag = "DeadUnit";
            Destroy(this.gameObject);
        }
            
	}

	public void calculateNextActTurn(int currentTurn) {
		this.nextActTurn = currentTurn + (int)Math.Ceiling(100.0f / this.speed);
	}

	public int CompareTo(object otherStats) {
		return nextActTurn.CompareTo (((UnitStats)otherStats).nextActTurn);
	}

	public bool isDead() {
		return this.dead;
	}

	public void receiveExperience(float experience) {
		this.currentExperience += experience;
	}
}
