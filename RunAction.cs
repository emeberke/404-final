﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Run")]

public class RunFromBattle : Action {

    [SerializeField]
    public GameObject damageTextPrefab;

	public override void Act (StateController controller) {
            Run(controller);
        }

    public void Run(StateController controller) {
		float randomNumber = Random.value;
		if (randomNumber < .3) {
         
            SceneManager.LoadScene ("PlayerRan");
		} else {
			GameObject.Find("TurnSystem").GetComponent<TurnSystem> ().nextTurn ();
		}
	}
}
