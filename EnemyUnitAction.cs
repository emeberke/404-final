﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyUnitAction : Action {

    [SerializeField]
	public string targetsTag;

    public GameObject owner;

    public GameObject target;

    public bool aiActive;

	GameObject findBestTarget() {
		GameObject[] possibleTargets = GameObject.FindGameObjectsWithTag (targetsTag);

        if (possibleTargets.Length > 0) {
            foreach(GameObject potentialTarget in possibleTargets) {
                UnitStats targetStats = potentialTarget.GetComponent<UnitStats>();
                
                if (targetStats.currentHealth < (.5 * targetStats.maxHealth)) {
                    return target = potentialTarget;
                }
                if (targetStats.defenseScore == targetStats.baseAC) {
                    return target = potentialTarget;
                }
                else {
                    int targetIndex = Random.Range (0, possibleTargets.Length);
			        target = possibleTargets [targetIndex];
                }
            }

			return target;
		}

		return null;
	}

    public override void Act(StateController controller) {

        aiActive = true;

    }
}
