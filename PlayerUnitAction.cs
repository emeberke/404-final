﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUnitAction : MonoBehaviour {

	[SerializeField]
	private GameObject physicalAttack;

	[SerializeField]
	private GameObject magicalAttack;

    [SerializeField]
    private GameObject defendAction;

	[SerializeField]
	public Sprite faceSprite;

    private GameObject currentAttack;

    private bool defending;
    private bool magicking;

	void Awake () {
		this.physicalAttack = Instantiate (this.physicalAttack, this.transform) as GameObject;
		this.magicalAttack = Instantiate (this.magicalAttack, this.transform) as GameObject;
        this.defendAction = Instantiate(this.defendAction, this.transform) as GameObject;

        this.currentAttack = this.physicalAttack;
	}

	public void selectMove(bool defense, bool magicAttack) {
        if(defense) {
            this.currentAttack = this.defendAction;
            this.defending = true;
        }
        if(magicAttack) {
            this.currentAttack = this.magicalAttack;
            this.magicking = true;
        }
        else {
            this.currentAttack = this.physicalAttack;
        }
	}

    public void act(GameObject target) {

	}

	public void updateHUD() {
		GameObject playerUnitFace = GameObject.Find ("PlayerUnitFace") as GameObject;
		playerUnitFace.GetComponent<Image> ().sprite = this.faceSprite;

		GameObject playerUnitHealthBar = GameObject.Find ("PlayerUnitHealthBar") as GameObject;
		playerUnitHealthBar.GetComponent<ShowUnitHealth> ().changeUnit (this.gameObject);

		GameObject playerUnitManaBar = GameObject.Find ("PlayerUnitManaBar") as GameObject;
		playerUnitManaBar.GetComponent<ShowUnitMana> ().changeUnit (this.gameObject);
	}
}
