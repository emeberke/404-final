using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Defend")]

    public class DefendAction : Action {
        public override void Act (StateController controller) {
            Defend(controller);
        }

        [SerializeField]
        private GameObject target;

        [SerializeField]
        private GameObject owner;

        public void Defend(StateController controller) {
            UnitStats ownerStats = this.owner.GetComponent<UnitStats>();
            UnitStats targetStats = target.GetComponent<UnitStats>();
            bool defender = true;

            ownerStats.defenseScore += 5;
            if(ownerStats.defenseScore > (5 + ownerStats.baseAC)) {
                ownerStats.defenseScore = 5 + ownerStats.baseAC;
            }

            targetStats.defending(defender);
        }
    }
    
    