using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadTown : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		SceneManager.LoadScene ("Town");
	}
}